package THREE;

public class Person implements Taxable{
	private String name;
	private double salary;
	
	public Person(String name,int salary){
		this.name = name;
		this.salary = salary;
	}
	
	public String getName(){
		return name;
	}
	public double getSalary(){
		return salary;
	}
	public double getTax() {
		long money = 500000;
		double tax = 0;
		if (money == 300000){
			tax = (5/100)*300000;
		}
		else if (money > 300000){
			long s = money - 300000;
			tax = (5/100)*300000 + (10/100)*s;
		}
		return tax;
	}
	
}
