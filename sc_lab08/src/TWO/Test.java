package TWO;


public class Test {
	public static void main(String[] args){
		BankAccount one = new BankAccount(0);
		BankAccount two = new BankAccount(10000);
		System.out.println("Min balance: " + one.getMeasure(two));
		
		Country Uruguay = new Country("Uruguay", 565612);
		Country Thailand = new Country("Thailand", 565612);
		System.out.println("Min area: " + Uruguay.getMeasure(Thailand));
		
		Person ploy = new Person("Ploy",190);
		Person toey = new Person("Toey",180);
		System.out.println("Min Height: " + ploy.getMeasure(toey));

		
	}

}
