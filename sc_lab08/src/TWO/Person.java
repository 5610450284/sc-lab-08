package TWO;

public class Person implements Measurable{
	private String name;
	private double height;
	
	public Person(String name,double height){
		this.name = name;
		this.height = height;
	}
	
	public String getName(){
		return name;
	}
	public double getHeight(){
		return height;
	}
	public int getMeasure(Object otherObject){
		Person other = (Person) otherObject;
		if (height < other.height){
			return -1;
		}
		if (height > other.height){
			return 1;
		}
		return 0;
	}

}
