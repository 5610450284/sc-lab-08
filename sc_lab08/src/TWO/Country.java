package TWO;

public class Country implements Measurable {
	private String name;
	private double area;
	
	public Country(String aName,double anArea){
		name = aName;
		area = anArea;
	}
	public String getName(){
		return name;
	}
	public double getArea(){
		return area;
	}
	public int getMeasure(Object otherObject){
		Country other = (Country) otherObject;
		if (area < other.area){
			return -1;
		}
		if (area > other.area){
			return 1;
		}
		return 0;
	}

}
