package TWO;

public class BankAccount implements Measurable{
	private int balance;
	public BankAccount(int balance) {
		this.balance = balance;
	}
	public int getMeasure(Object otherObject){
		BankAccount other = (BankAccount) otherObject;
		if (balance < other.balance){
			return -1;
		}
		if (balance > other.balance){
			return 1;
		}
		return 0;
	}

}
